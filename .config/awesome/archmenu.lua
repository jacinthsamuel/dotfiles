 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"Minder", "com.github.phase1geo.minder ", "/usr/share/icons/hicolor/16x16/apps/com.github.phase1geo.minder.svg" },
     {"Mousai", "mousai"},
     {"Notepadqq", "notepadqq ", "/usr/share/icons/hicolor/16x16/apps/notepadqq.png" },
     {"Vim", "xterm -e vim ", "/usr/share/icons/hicolor/48x48/apps/gvim.png" },
     {"nitrogen", "nitrogen", "/usr/share/icons/hicolor/16x16/apps/nitrogen.png" },
     {"picom", "picom"},
 }

 local menude7a22a0c94aa64ba2449e520aa20c99 = {
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
 }

 local menu251bd8143891238ecedc306508e29017 = {
     {"0 A.D.", "0ad", "/usr/share/pixmaps/0ad.png" },
 }

 local menud334dfcea59127bedfcdbe0a3ee7f494 = {
     {"Flameshot", "/usr/bin/flameshot", "/usr/share/icons/hicolor/48x48/apps/org.flameshot.Flameshot.png" },
     {"Inkscape", "inkscape ", "/usr/share/icons/hicolor/16x16/apps/org.inkscape.Inkscape.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"Okular", "okular ", "/usr/share/icons/hicolor/16x16/apps/okular.png" },
     {"XDvi", "xdvi "},
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Avahi SSH Server Browser", "/usr/bin/bssh"},
     {"Avahi VNC Server Browser", "/usr/bin/bvnc"},
     {"Chromium", "/usr/bin/chromium ", "/usr/share/icons/hicolor/16x16/apps/chromium.png" },
     {"Firefox", "/usr/lib/firefox/firefox ", "/usr/share/icons/hicolor/16x16/apps/firefox.png" },
     {"LibreWolf", "/usr/lib/librewolf/librewolf ", "/usr/share/icons/hicolor/16x16/apps/librewolf.png" },
     {"Signal", "signal-desktop -- ", "/usr/share/icons/hicolor/16x16/apps/signal-desktop.png" },
     {"Telegram Desktop", "telegram-desktop -- ", "/usr/share/icons/hicolor/16x16/apps/telegram.png" },
     {"Tor Browser", "torbrowser-launcher ", "/usr/share/icons/hicolor/128x128/apps/torbrowser.png" },
     {"Tor Browser Launcher Settings", "torbrowser-launcher --settings", "/usr/share/icons/hicolor/128x128/apps/torbrowser.png" },
     {"Waterfox G4", "waterfox-g4 ", "/usr/share/icons/hicolor/16x16/apps/waterfox-g4.png" },
     {"qBittorrent", "qbittorrent ", "/usr/share/icons/hicolor/16x16/apps/qbittorrent.png" },
 }

 local menudf814135652a5a308fea15bff37ea284 = {
     {"Joplin", "env DESKTOPINTEGRATION=false /usr/bin/joplin-desktop --no-sandbox "},
     {"LibreOffice", "libreoffice ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-startcenter.png" },
     {"LibreOffice Base", "libreoffice --base ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-base.png" },
     {"LibreOffice Calc", "libreoffice --calc ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-calc.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"LibreOffice Impress", "libreoffice --impress ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-impress.png" },
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"LibreOffice Writer", "libreoffice --writer ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-writer.png" },
     {"Minder", "com.github.phase1geo.minder ", "/usr/share/icons/hicolor/16x16/apps/com.github.phase1geo.minder.svg" },
     {"Okular", "okular ", "/usr/share/icons/hicolor/16x16/apps/okular.png" },
 }

 local menue6f43c40ab1c07cd29e4e83e4ef6bf85 = {
     {"Emacs", "emacs ", "/usr/share/icons/hicolor/16x16/apps/emacs.png" },
     {"Geany", "geany ", "/usr/share/icons/hicolor/16x16/apps/geany.png" },
     {"Minder", "com.github.phase1geo.minder ", "/usr/share/icons/hicolor/16x16/apps/com.github.phase1geo.minder.svg" },
     {"Notepadqq", "notepadqq ", "/usr/share/icons/hicolor/16x16/apps/notepadqq.png" },
     {"Qt Assistant", "assistant", "/usr/share/icons/hicolor/32x32/apps/assistant.png" },
     {"Qt Designer", "designer ", "/usr/share/icons/hicolor/128x128/apps/QtProject-designer.png" },
     {"Qt Linguist", "linguist ", "/usr/share/icons/hicolor/16x16/apps/linguist.png" },
     {"Qt QDBusViewer ", "qdbusviewer", "/usr/share/icons/hicolor/32x32/apps/qdbusviewer.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"Audacious", "audacious ", "/usr/share/icons/hicolor/48x48/apps/audacious.png" },
     {"Kdenlive", "kdenlive ", "/usr/share/icons/hicolor/16x16/apps/kdenlive.png" },
     {"OBS Studio", "obs", "/usr/share/icons/hicolor/128x128/apps/com.obsproject.Studio.png" },
     {"PulseAudio Volume Control", "pavucontrol"},
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"Qt V4L2 video capture utility", "qvidcap", "/usr/share/icons/hicolor/16x16/apps/qvidcap.png" },
     {"SimpleScreenRecorder", "simplescreenrecorder --logfile", "/usr/share/icons/hicolor/16x16/apps/simplescreenrecorder.png" },
     {"Tenacity", "env UBUNTU_MENUPROXY=0 tenacity ", "/usr/share/icons/hicolor/16x16/apps/tenacity.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
     {"Volctl", "volctl"},
     {"mpv Media Player", "mpv --player-operation-mode=pseudo-gui -- ", "/usr/share/icons/hicolor/16x16/apps/mpv.png" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Alacritty", "alacritty", "/usr/share/pixmaps/Alacritty.svg" },
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover"},
     {"File Manager PCManFM", "pcmanfm "},
     {"Hardware Locality lstopo", "lstopo"},
     {"Htop", "xterm -e htop", "/usr/share/pixmaps/htop.png" },
     {"Manage Printing", "/usr/bin/xdg-open http://localhost:631/", "/usr/share/icons/hicolor/16x16/apps/cups.png" },
     {"Oracle VM VirtualBox", "VirtualBox ", "/usr/share/icons/hicolor/16x16/mimetypes/virtualbox.png" },
     {"Vifm", "xterm -e vifm ", "/usr/share/pixmaps/vifm.png" },
     {"Virtual Machine Manager", "virt-manager", "/usr/share/icons/hicolor/16x16/apps/virt-manager.png" },
     {"fish", "xterm -e fish", "/usr/share/pixmaps/fish.png" },
 }

xdgmenu = {
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Education", menude7a22a0c94aa64ba2449e520aa20c99},
    {"Games", menu251bd8143891238ecedc306508e29017},
    {"Graphics", menud334dfcea59127bedfcdbe0a3ee7f494},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Office", menudf814135652a5a308fea15bff37ea284},
    {"Programming", menue6f43c40ab1c07cd29e4e83e4ef6bf85},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}

