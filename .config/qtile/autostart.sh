#!/usr/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run feh --bg-scale --randomize ~/wallpapers/walls/*
run nm-applet
run lxsession
run numlockx on
run emacs --daemon &
run volctl
run telegram-desktop
run pavucontrol
run kdeconnectd
run kdeconnect-app
run syncthing
run blueman-manager
run qbittorrent
