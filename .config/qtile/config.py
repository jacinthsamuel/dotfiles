import os
import subprocess
import collections
from libqtile.backend.base import Static
from libqtile import bar, layout, qtile, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Rule, KeyChord
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal, logger

mod = "mod4"
terminal = guess_terminal()

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawn("dmenu_run -c -l 20"), desc="Spawn a command using a prompt widget"),
    Key([mod], "p", lazy.spawn("passmenu -c -l 20"), desc="launch passmenu"),
    Key([mod], "e", lazy.spawn("emacsclient -c"), desc="launch passmenu"),
    Key([mod, "shift"], "p", lazy.spawn("pcmanfm"), desc="launch pcmanfm"),
    Key([], "Print", lazy.spawn("flameshot gui"), desc="Take Screenshot"),
    Key([mod], "Print", lazy.spawn("flameshot screen"), desc="Take Screenshot"),

KeyChord([mod],"b", [
    Key([], "q", lazy.spawn("qutebrowser"), desc="Launch Qutebrowser"),
    Key([], "f", lazy.spawn("firefox"), desc="Launch Firefox"),
    Key([], "v", lazy.spawn("vivaldi-stable"), desc="Launch Vivaldi")
    ]),

KeyChord([mod],"v", [
    Key([], "k", lazy.spawn("kdenlive"), desc="Launch Kdenlive"),
    Key([], "g", lazy.spawn("gimp"), desc="Launch Gimp"),
    Key([], "a", lazy.spawn("audacity"), desc="Launch Audacity")
    ])
]

for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]
group_labels = ["ter", "web", "fil", "vir", "edi", "soc", "aud", "tls", "oth",]
group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend(
        [
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),

            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )

layout_theme = {"border_width": 2,
                "margin": 4,
                "border_focus": "#e5b567",
                "border_normal": "#d6d6d6"
                }

layouts = [
    layout.Columns(**layout_theme),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(),
    # layout.RatioTile(),
    layout.Tile(**layout_theme),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="Source Code Pro Semi-bold",
    fontsize=12,
    padding=3,
    background="2e2e2e",
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(
                                background = "d6d6d6",
                                foreground = "2e2e2e"),
                widget.GroupBox(
                                active = "e87d3e",
                                inactive = "d6d6d6",
                                this_current_screen_border = "b4d273"
                                ),
                widget.Prompt(),
                widget.WindowName(
                    foreground = "e5b567",
                    fontsize = 16
                ),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Clock(
                    foreground = "#d6d6d6",
                    format="%Y-%m-%d %a %I:%M %p"),
                widget.Systray(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = [
    Rule(Match(wm_class=['Emacs']), group="1"),
    Rule(Match(wm_class=['qutebrowser', 'Opera', 'Tor Browser', 'Vivaldi-stable']), group="2"),
    Rule(Match(wm_class=['Pcmanfm']), group="3"),
    Rule(Match(wm_class=['Virt-manager']), group="4"),
    Rule(Match(wm_class=['kdenlive', 'Audacity', 'Gimp']), group="5"),
    Rule(Match(wm_class=['TelegramDesktop']), group="6"),
    Rule(Match(wm_class=['Blueman-manager', 'pavucontrol']), group="7"),
    Rule(Match(wm_class=['kdeconnect.app', 'SimpleScreenRecorder']), group="8"),
    Rule(Match(wm_class=['qBittorrent']), group="9"),
]
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),
        Match(wm_class="makebranch"),
        Match(wm_class="maketag"),
        Match(wm_class="ssh-askpass"),
        Match(wm_class="Pinentry-gtk-2"),
        Match(title="branchdialog"),
        Match(title="pinentry"),
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_minimize = True

wl_input_rules = None

wl_xcursor_theme = None
wl_xcursor_size = 24

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
