set fish_greeting

pfetch
fortune -s | cowsay -g

#Random Color Scripts
#colorscript random

# PAths
fish_add_path ~/.config/emacs/bin

###ALIASES
#doom emacs
alias doomsync="~/.config/emacs/bin/doom sync"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"

#native commands
alias ls="eza -lah"
alias grep="rg"
alias find="sudo find"
alias modules-load="sudo modules-load"
alias .1="cd .."
alias .2="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."

##package manager
#xbps
alias xbpsi="sudo xbps-install"
alias xbpsr="sudo xbps-remove"
alias void-update="sudo xbps-install -Su"

#nixos
alias nixos-update="sudo nixos-rebuild switch --upgrade"
alias nixos-clean="sudo nix-collect-garbage -d"
alias nixos-copy="sudo cp -i /etc/nixos/configuration.nix ~/nixos/configuration.nix"
alias nixos-rebuild="sudo nixos-rebuild switch"
alias nixos-channel-list="sudo nix-channel --list"
alias nixos-channel-updtae="sudo nix-channel --update"

#pacman
alias pacs="sudo pacman -S"
alias pacr="sudo pacman -R"
alias arch-update="sudo pacman -Syyu"
alias bashrc="vim .bashrc"

# VOid linux
alias xbpsi="sudo xbps-install"
alias xbpsr="sudo xbps-remove"
alias void-update="sudo xbps-install -Su"

#power
alias shutdown="sudo shutdown -h now"

#protonvpn
alias spcr="sudo protonvpn c -r"
alias spd="sudo protonvpn d"

#safety
alias cp="cp -i"
alias copy-awesome-config="sudo cp -ir  ~/.config/awesome ~/dotfiles/.config/"

#utilities
alias ln="sudo ln"
alias usermod="sudo usermod"
alias useradd="sudo useradd"
alias restore1brightness="xrandr --output HDMI-1 --brightness 1"
alias restore2brightness="xrandr --output VGA-1 --brightness 1"
alias half1-brightness="xrandr --output HDMI-1 --brightness 0.5"
alias half2-brightness="xrandr --output VGA-1 --brightness 0.5"
alias set-wallpaper="feh --bg-scale --randomize ~/wallpapers/walls/*"
alias kitty-themes="kitty +kitten themes"
alias fonts-copy="sudo cp -r /usr/share/fonts/* ~/fonts/"
alias ratemirrors="rate-mirrors --protocol https arch | sudo tee /etc/pacman.d/mirrorlist"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias git-bare="git --git-dir=$HOME/dotfiles --work-tree=$HOME"
alias clock="tty-clock -cBsSC 2"

#config files
alias qb-config="vim ~/.config/qutebrowser/config.py"
alias nixos-config="sudo vim /etc/nixos/configuration.nix"
alias pacman-config="sudo vim /etc/pacman.conf"
alias fish-config="vim ~/.config/fish/config.fish"
alias awesome-config="vim ~/.config/awesome/rc.lua"
alias copy-songs="cp -v ~/temp_songs/*.mp3 ~/songs/"

#yay
alias yayupdate="yay -Syu --devel --timeupdate"

#youtube-dl
alias ytdl="youtube-dl"
alias ytdlba="youtube-dl -x -f bestaudio"
alias ytdla="youtube-dl -x"


#flatpak
set -l xdg_data_home $XDG_DATA_HOME ~/.local/share
set -gx --path XDG_DATA_DIRS $xdg_data_home[1]/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share

for flatpakdir in ~/.local/share/flatpak/exports/bin /var/lib/flatpak/exports/bin
    if test -d $flatpakdir
        contains $flatpakdir $PATH; or set -a PATH $flatpakdir
    end
end

#MODE#

#vi mode
#function fish_user_key_bindings
#  fish_vi_key_bindings
#end

#
#starship prompt
starship init fish | source
