#!/usr/bin/env python3

import dracula.draw

# Load existing settings made via :set
config.load_autoconfig()

dracula.draw.blood(c, {
    'spacing': {
        'vertical': 6,
        'horizontal': 8
    }
})

#dark mode
config.set("colors.webpage.darkmode.enabled", True)

#keybindings

config.bind('<Ctrl-Shift-m>', 'spawn mpv {url}')
config.bind('<Ctrl-m>', 'hint links spawn mpv {hint-url}')
