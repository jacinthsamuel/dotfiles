(setq user-full-name "Jacinth Samuel"
      user-mail-address "jjsamuel1996@gmail.com")

(setq doom-font (font-spec :family "Source Code Pro" :size 18 :weight 'semi-bold)
      doom-variable-pitch-font (font-spec :family "Ubuntu Mono" :size 18))

(setq doom-theme 'doom-one)

(setq display-line-numbers-type t)

(setq org-directory "~/org/")

(use-package org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode))
