# .bashrc

pfetch
fortune -s | cowsay -g

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls="exa -lah"
alias grep="rg"
alias .1="cd .."
alias .2="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."

alias xbpsi="sudo xbps-install"
alias xbpsr="sudo xbps-remove"
alias void-update="sudo xbps-install -Su"
alias void-clean="sudo xbps-remove -Oo"

alias nixos-update="sudo nixos-rebuild switch --upgrade"
alias nixos-clean="sudo nix-collect-garbage -d"
alias nixos-rebuild="sudo nixos-rebuild switch"
alias nixos-channel-list="sudo nix-channel --list"
alias nixos-channel-updtae="sudo nix-channel --update"

alias pacs="sudo pacman -S"
alias pacr="sudo pacman -R"
alias arch-update="sudo pacman -Syyu"

alias shutdown="sudo shutdown -h now"

alias cp="cp -i"

alias restore1brightness="xrandr --output HDMI-1 --brightness 1"
alias restore2brightness="xrandr --output VGA-1 --brightness 1"
alias half1-brightness="xrandr --output HDMI-1 --brightness 0.5"
alias half2-brightness="xrandr --output VGA-1 --brightness 0.5"
alias set-wallpaper="feh --bg-scale --randomize ~/wallpapers/walls/*"
alias kitty-themes="kitty +kitten themes"

alias ratemirrors="rate-mirrors --protocol https arch | sudo tee /etc/pacman.d/mirrorlist"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

alias ytdl="yt-dlp"
alias ytdla="yt-dlp -x"

PS1='[\u@\h \W]\$ '

set -o vi

eval "$(starship init bash)"
